package com.example.emerson.reversas;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumero;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Map<Integer, Button> mapa = new HashMap<Integer, Button>();
    private Button btnSalvar;
    private Button btnLiberar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumero = (EditText) findViewById(R.id.txtNumero);



        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnSalvar = (Button) findViewById(R.id.idBtnSalvar);
        btnLiberar = (Button) findViewById(R.id.idBtnLiberar);

        btnLiberar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtNumero.getText().toString().length() > 0 && txtNumero.getText().toString().matches("^[0-9]*$")){
                    Button btn =  mapa.get(Integer.valueOf(txtNumero.getText().toString()));
                    if(btn != null){
                        btn.setEnabled(true);
                        LinearLayout linear = (LinearLayout) btn.getParent();
                        linear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        txtNumero.setText("");
                    }
                }
            }
        });

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor edit = preferences.edit();
                for( int i = 1; i <= 9; i++){
                    boolean disponivel = false;
                    Button btn =  mapa.get(i);
                    if(btn.isEnabled()){
                        disponivel = true;
                    }

                    edit.putBoolean("mesa_" + i, disponivel);
                }
                edit.apply();
            }
        });

        cadastrarListener(btn1);
        cadastrarListener(btn2);
        cadastrarListener(btn3);
        cadastrarListener(btn4);
        cadastrarListener(btn5);
        cadastrarListener(btn6);
        cadastrarListener(btn7);
        cadastrarListener(btn8);
        cadastrarListener(btn9);

        mapa.put(1, btn1);
        mapa.put(2, btn2);
        mapa.put(3, btn3);
        mapa.put(4, btn4);
        mapa.put(5, btn5);
        mapa.put(6, btn6);
        mapa.put(7, btn7);
        mapa.put(8, btn8);
        mapa.put(9, btn9);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        for( int i = 1; i <= 9; i++){
            boolean disponivel = preferences.getBoolean("mesa_" + i, true);
            if(!disponivel){
                Button btn = mapa.get(i);
                btn.setEnabled(disponivel);
                LinearLayout linear = (LinearLayout) btn.getParent();
                linear.setBackgroundColor(getResources().getColor(R.color.colorRed));
            }
        }
    }


    private void cadastrarListener(Button btn){
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setEnabled(false);
                LinearLayout linear = (LinearLayout) view.getParent();
                linear.setBackgroundColor(getResources().getColor(R.color.colorRed));
            }
        });
    }
}
